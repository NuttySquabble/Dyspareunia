﻿using RimWorld;
using System;
using UnityEngine;
using Verse;

namespace Dyspareunia
{
    public class Mod : Verse.Mod
    {
        internal static Settings Settings;

        public Mod(ModContentPack content) : base(content) => Settings = GetSettings<Settings>();

        public override void DoSettingsWindowContents(Rect rect)
        {
            Listing_Standard content = new Listing_Standard();
            content.Begin(rect);
            content.Label($"Damage Factor: {Settings.damageFactor.ToStringPercent()}", tooltip: "Percentage of damage taken from rubbing and stretch, compared to default values");
            Settings.damageFactor = (float)Math.Round(content.Slider(Settings.damageFactor, 0, 2), 2);
            content.CheckboxLabeled("Indestructible Parts", ref Settings.indestructibleParts, "Parts can't be completely destroyed by sex and childbirth");
            content.Label($"Stretch Factor: {Settings.stretchFactor.ToStringPercent()}", tooltip: "Percentage of organ stretch from sex and childbirth");
            Settings.stretchFactor = (float)Math.Round(content.Slider(Settings.stretchFactor, 0, 2), 2);
            content.Label($"1% Contraction Time: {Settings.contractionPercentInterval / GenDate.TicksPerHour} hours", tooltip: "How long it takes for organs (vagina and anus) to naturally contract by 1% of their maximum size (0 to disable contraction)");
            Settings.contractionPercentInterval = Mathf.RoundToInt(content.Slider(Settings.contractionPercentInterval / GenDate.TicksPerHour, 0, 24)) * GenDate.TicksPerHour;
            if (Settings.contractionPercentInterval > 0)
            {
                content.CheckboxLabeled("Natural Contraction", ref Settings.naturalContraction, "Contract organs to their original size, otherwise set the target size manually");
                if (!Settings.naturalContraction)
                {
                    content.Label($"Contraction Size Target: {Settings.contractionTarget.ToStringPercent()}", tooltip: "Contract parts to this size. Sizes:\n- Micro: 0-1%\n- Small: 1-20%\n- Average: 40-60%\n- Large: 60-80%\n- Huge: 80-100%");
                    Settings.contractionTarget = (float)Math.Round(content.Slider(Settings.contractionTarget, 0, 1), 2);
                }
            }
            content.CheckboxLabeled("Debug Logging", ref Settings.debugLogging, "Enable verbose logging, use to report bugs");
            content.End();
        }

        public override string SettingsCategory() => "Dyspareunia";
    }
}
