﻿using System;
using System.Linq;
using Verse;

namespace Dyspareunia
{
    public class HediffComp_Orifice : HediffComp
    {
        public float target = 0.5f;

        public override void CompPostPostAdd(DamageInfo? dinfo)
        {
            base.CompPostPostAdd(dinfo);
            target = parent.Severity;
        }

        public override void CompPostTick(ref float severityAdjustment)
        {
            base.CompPostTick(ref severityAdjustment);

            // If Contraction Interval setting is set to 0, contraction is disabled
            if (Mod.Settings.contractionPercentInterval <= 0)
                return;

            // Runs when contraction by 1% is needed
            if (!Pawn.IsHashIntervalTick(Mod.Settings.contractionPercentInterval))
                return;

            // Skip unspawned pawns
            if (!Pawn.Spawned)
                return;

            // Only contract organs above target severity
            float stretchAmount = parent.Severity - (Mod.Settings.naturalContraction ? target : Mod.Settings.contractionTarget);
            if (stretchAmount <= 0)
                return;

            // HydraulicVagina and HydraulicAnus don't contract
            if (parent.def.defName == "HydraulicVagina" || parent.def.defName == "HydraulicAnus")
                return;

            // If current orifice is an anus, checking if there is an anal plug from rjw-ex mod
            if (parent.IsAnus() && Pawn?.apparel != null && Pawn.apparel.WornApparel.Any(a => a.def.thingClass.FullName == "rjwex.anal_plug"))
            {
                Dyspareunia.Log($"A plug is found in {Pawn}'s {parent}. No contraction.");
                return;
            }

            // Contract the part by 1%
            Dyspareunia.Log($"{Pawn}'s {parent.LabelCap} (severity {parent.Severity:f3}) contracts by 1%.");
            severityAdjustment -= Math.Min(0.01f, stretchAmount);
        }

        public override void CompExposeData()
        {
            Scribe_Values.Look(ref target, "target", 0.5f);
        }

        public override string CompDebugString()
        {
            return base.CompDebugString() + $"\nSize: {parent.GetOrganSize():f3}\nTarget severity: {target:f3}";
        }
    }
}
